import { MongoClient, ObjectId } from 'mongodb';
import Gallery from "@/components/UI/Gallery";


const Post = (props) => {
    return (
        <Gallery title={props.post.text}/>
    );
}

export default Post;


export const getStaticPaths = async () => {
    const client = await MongoClient.connect('mongodb+srv://kate:katePassword@atlascluster.guqc6ch.mongodb.net/?retryWrites=true&w=majority');
    const db = client.db('main');
    const blogCollection = db.collection('blog');
    const postIds = await blogCollection.find({}, { type: 1 }).toArray();
    client.close();

    return {
        fallback: false,
        paths: postIds.map(post => ({
            params: {
                postId: post._id.toString(),
            }
        })),
    };
}

export async function getStaticProps(context) {
    const id = context.params.postId;
    const client = await MongoClient.connect('mongodb+srv://kate:katePassword@atlascluster.guqc6ch.mongodb.net/?retryWrites=true&w=majority');
    const db = client.db('main');
    const blogCollection = db.collection('blog');
    const post = await blogCollection.findOne({ _id: new ObjectId(id) });

    const transformedPost = {
        id: post._id.toString(),
        place: post.place,
        title: post.title,
        titleImg: post.titleImg,
        images: post.images,
        text: post.text,
    };

    return {
        props: {
            post: transformedPost,
        },
    };
}