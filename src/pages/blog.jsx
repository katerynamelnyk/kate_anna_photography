import { MongoClient } from "mongodb";

import BlogBlock from "@/components/Blog/BlogBlock";

const Blog = (props) => {
  return (
    <BlogBlock posts={props.posts} />
  );
}

export default Blog;

export async function getStaticProps() {
  const client = await MongoClient.connect('mongodb+srv://kate:katePassword@atlascluster.guqc6ch.mongodb.net/?retryWrites=true&w=majority');
  const db = client.db('main');
  const blogCollection = db.collection('blog');
  const posts = await blogCollection.find().toArray();

  const transformedPosts = posts.map(blog => ({
    id: blog._id.toString(),
    place: blog.place,
    title: blog.title,
    titleImg: blog.titleImg,
  }));

  return {
    props: {
      posts: transformedPosts,
    },
  };
}