import { MongoClient, ObjectId } from 'mongodb';

import Gallery from '@/components/UI/Gallery';

const PhotoShooting = (props) => {
    return <Gallery
        images={props.session?.images}
        title={props.session?.name}
    />
}

export default PhotoShooting;



export const getStaticPaths = async () => {
    const client = await MongoClient.connect('mongodb+srv://kate:katePassword@atlascluster.guqc6ch.mongodb.net/?retryWrites=true&w=majority');
    const db = client.db('main');
    const sessionsCollection = db.collection('sessions');
    const sessionsIds = await sessionsCollection.find({}, { _id: 1 }).toArray();
    client.close();

    return {
        fallback: false,
        paths: sessionsIds.map(session => ({
            params: {
                sessionId: session._id.toString(),
            },
        })),
    }
}

export const getStaticProps = async (context) => {
    const currentId = context.params.sessionId;
    const client = await MongoClient.connect('mongodb+srv://kate:katePassword@atlascluster.guqc6ch.mongodb.net/?retryWrites=true&w=majority');
    const db = client.db('main');
    const sessionsCollection = db.collection('sessions');
    const session = await sessionsCollection.findOne({ _id: new ObjectId(currentId) });
    client.close();

    return {
        props: {
            session: {
                id: session._id.toString(),
                name: session.name,
                place: session.place,
                cover: session.cover,
                images: session.images,
            }
        }
    }
}