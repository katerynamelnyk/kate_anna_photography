import { MongoClient } from "mongodb";
import Home from "@/components/Home";

const HomePage = (props) => {
  return <Home sessions={props.sessions} />
}

export default HomePage;



export async function getStaticProps() {
  const client = await MongoClient.connect('mongodb+srv://kate:katePassword@atlascluster.guqc6ch.mongodb.net/?retryWrites=true&w=majority');
  const db = client.db('main');
  const sessionsCollection = db.collection('sessions');
  const sessions = await sessionsCollection.find().toArray();
  client.close();

  const transformedSessions = sessions.map(session => ({
    id: session._id.toString(),
    name: session.name,
    place: session.place,
    cover: session.cover,
    images: session.images,
  }))

  return {
    props: {
      sessions: transformedSessions,
    }
  }
}