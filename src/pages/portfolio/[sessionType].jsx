import { MongoClient } from 'mongodb';

import Gallery from '@/components/UI/Gallery';

const SessionType = (props) => {
    return <Gallery
        images={props.portfolio?.images}
        title={props.portfolio?.title}
    />
}

export default SessionType;

export const getStaticPaths = async () => {
    const client = await MongoClient.connect('mongodb+srv://kate:katePassword@atlascluster.guqc6ch.mongodb.net/?retryWrites=true&w=majority');
    const db = client.db('main');
    const portfolioCollection = db.collection('portfolio');
    const portfolioTypes = await portfolioCollection.find({}, { type: 1 }).toArray();
    client.close();

    return {
        fallback: false,
        paths: portfolioTypes.map(portfolio => ({
            params: {
                sessionType: portfolio.type,
            }
        }))
    }
}

export const getStaticProps = async (context) => {
    const currentType = context.params.sessionType;
    const client = await MongoClient.connect('mongodb+srv://kate:katePassword@atlascluster.guqc6ch.mongodb.net/?retryWrites=true&w=majority');
    const db = client.db('main');
    const portfolioCollection = db.collection('portfolio');
    const portfolio = await portfolioCollection.findOne({ type: currentType });
    client.close();

    return {
        props: {
            portfolio: {
                type: portfolio.type,
                images: portfolio.images,
                title: portfolio.title,
            }
        }
    }
}