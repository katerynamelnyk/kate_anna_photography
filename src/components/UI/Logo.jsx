import localFont from 'next/font/local';
import classes from './Logo.module.css';

const myFont = localFont({ src: '../../../public/SCRIPTIN.ttf'});

const Logo = () => {
    const logoNameClasses = myFont.className + ' ' + classes.name;
    return (
        <div className={classes.container}>
            <div className={logoNameClasses}>Kate & Anna</div>
            <div className={classes.title}>PHOTOGRAPHY</div>
        </div>
    )
}

export default Logo;