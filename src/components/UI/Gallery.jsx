import Image from 'next/image';
import localFont from 'next/font/local';

import classes from './Gallery.module.css';

const myFont = localFont({ src: '../../../public/SCRIPTIN.ttf' });

//add 'H' to the name of image if photo is horizontal orientation
// 1700px on longest side

const Gallery = (props) => {
    const images = props?.images?.map(img =>
        <Image
            className={
                img.includes('H')
                    ? `${classes.img} ${classes['img-horizontal']}`
                    : classes.img
            }
            key={img}
            src={img}
            width={img.includes('H') ? 1000 : 500}
            height={img.includes('H') ? 667 : 720}
            alt={img}
        />
    );

    const titleClasses = myFont.className + ' ' + classes.title;

    return (
        <div className={classes.container}>
            <h2 className={titleClasses}>{props.title}</h2>
            {images}
        </div>
    )
}

export default Gallery;