import Image from 'next/image';

import classes from './BlogItem.module.css';


const BlogItem = (props) => {
    return (
        <div>
            <div className={classes['image-wrapper']}>
                <Image
                    src={props.titleImg}
                    width={500}
                    height={500}
                    alt="place"
                />
                <div className={classes.place}>
                    {props.place}
                </div>
            </div>
            <div>
                {props.title}
            </div>
            <button
                className={classes.button}
                onClick={props.onClick}
            >
                Read More
            </button>
        </div>
    );
}

export default BlogItem;