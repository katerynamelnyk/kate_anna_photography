import { useRouter } from 'next/router';
import BlogItem from './BlogItem';

import classes from './BlogBlock.module.css';

const BlogBlock = (props) => {
    const router = useRouter();
    
    const onClickHandler = (id) => {
        router.push(`/blog/${id}`);
    };

    const blogItems = props.posts
        .map(item => <BlogItem
            key={item.id}
            titleImg={item.titleImg}
            place={item.place}
            title={item.title}
            onClick={() => onClickHandler(item.id)}
        />);

    return (
        <div className={classes.container}>
            <div className={classes.title}>
                <p className={classes.text}>BLOG</p>
            </div>
            <div className={classes.items}>
                {blogItems}
            </div>
        </div>
    );
}

export default BlogBlock;