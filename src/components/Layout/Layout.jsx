import { Lekton } from 'next/font/google';
import Link from 'next/link';
import { useRouter } from 'next/router';

import Logo from '../UI/Logo';
import classes from './Layout.module.css';

const lekton = Lekton({ subsets: ['latin'], weight: '400' });

const Layout = (props) => {
    const router = useRouter();

    return (
        <div className={lekton.className + ' ' + classes['main-container']}>
            <header>
                <nav className={classes.navigation}>
                    <div className={classes['logo-container']}>
                        <Link href="/">
                            <Logo />
                        </Link>
                    </div>
                    <div className={classes['links-container']}>
                        <Link
                            href="/"
                            className={router.pathname === '/'
                                ? `${classes.active}` : `${classes.normal}`}
                        >
                            HOME
                        </Link>
                        <Link
                            href="/portfolio"
                            className={router.pathname === '/portfolio'
                                ? `${classes.active}` : `${classes.normal}`}
                        >
                            PORTFOLIO
                        </Link>
                        <Link
                            href="/about"
                            className={router.pathname === '/about'
                                ? `${classes.active}` : `${classes.normal}`}
                        >
                            ABOUT
                        </Link>
                        <Link
                            href="/information"
                            className={router.pathname === '/information'
                                ? `${classes.active}` : `${classes.normal}`}
                        >
                            INFORMATION
                        </Link>
                        <Link
                            href="/blog"
                            className={router.pathname === '/blog'
                                ? `${classes.active}` : `${classes.normal}`}
                        >
                            BLOG
                        </Link>
                        {/* <Link
                            href="/contact"
                            className={router.pathname === '/contact'
                                ? `${classes.active}` : `${classes.normal}`}
                        >
                            CONTACT
                        </Link> */}
                    </div>
                </nav>
            </header>
            <div>{props.children}</div>
        </div>
    )
}

export default Layout;