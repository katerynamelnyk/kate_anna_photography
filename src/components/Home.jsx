import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

import classes from './Home.module.css';


const Home = (props) => {
    const mainPhotos = props.sessions;

    const router = useRouter();

    const [activePhoto, setActivePhoto] = useState(0);
    const onClickNextHandler = () => {
        setActivePhoto(prev => prev < mainPhotos.length - 1 ? prev + 1 : 0);
    };
    const onClickBackHandler = () => {
        setActivePhoto(prev => prev === 0 ? mainPhotos.length - 1 : prev - 1);
    };

    const backgroundStyle = {
        backgroundImage: `url(${mainPhotos[activePhoto].cover})`,
    };

    useEffect(() => {
        const timer = setTimeout(() => {
            setActivePhoto(prev => prev < mainPhotos.length - 1 ? prev + 1 : 0);
        }, 15000);
        return () => clearTimeout(timer);
    }, [activePhoto]);


    const onClickToPhotoShootingHandler = (event) => {
        event.preventDefault();
        router.push(`/${mainPhotos[activePhoto].id}`);
    };

    return (
        <>
            <div
                className={classes.container}
                style={backgroundStyle}
            >
                <div className={classes.helper}></div>
                <div className={classes['navigation-buttons']}>
                    <button onClick={onClickBackHandler}>
                        <svg width="44" height="66" viewBox="0 0 44 66" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <line x1="43.143" y1="0.396655" x2="0.304329" y2="33.2729" stroke="white" />
                            <line y1="-0.5" x2="54" y2="-0.5" transform="matrix(-0.793309 -0.608819 -0.608819 0.793309 42.8386 66)" stroke="white" />
                        </svg>
                    </button>
                    <button onClick={onClickNextHandler}>
                        <svg width="44" height="66" viewBox="0 0 44 66" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <line x1="0.695591" y1="65.6033" x2="43.5343" y2="32.7271" stroke="white" />
                            <line y1="-0.5" x2="54" y2="-0.5" transform="matrix(0.793309 0.608819 0.608819 -0.793309 1 0)" stroke="white" />
                        </svg>
                    </button>
                </div>
                <div className={classes.info}>
                    <div className={classes.place}>
                        {mainPhotos[activePhoto].place}
                    </div>
                    <button onClick={onClickToPhotoShootingHandler}>
                        {mainPhotos[activePhoto].name}
                    </button>
                    <div>
                        
                    </div>
                </div>
            </div>
        </>
    );
}

export default Home;