import Image from 'next/image';
import { useRouter } from 'next/router';

import classes from './PortfolioBlock.module.css';

//images for covers should be cropped 4/5(8/10)

const blocksData = [
    {
        cover: '/portfolio-covers/weddings.jpg',
        type: 'weddings',
        title: 'Weddings',
    },
    {
        cover: '/portfolio-covers/love-stories.jpg',
        type: 'loveStories',
        title: 'Love stories',
    },
    {
        cover: '/portfolio-covers/family.jpg',
        type: 'family',
        title: 'Families',
    },
    {
        cover: '/portfolio-covers/individual.jpg',
        type: 'individual',
        title: 'Individual',
    },
]

const PortfolioBlock = () => {
    const router = useRouter();

    const onClickHandler = (sessionType) => {
        router.push(`/portfolio/${sessionType}`);
    };

    const blocks = blocksData.map(item => {
        return (
            <div
                className={`${classes.item} ${classes.special}`}
                key={item.type}
                onClick={() => onClickHandler(`${item.type}`)}
            >
                <Image
                    src={item.cover}
                    width={300}
                    height={375}
                    alt={item.type}
                />
                <p>
                    {item.title}
                    <svg width="17" height="28" viewBox="0 0 17 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <line y1="-0.5" x2="21.2257" y2="-0.5" transform="matrix(0.753802 -0.657102 0.559837 0.828603 1 28)" stroke="white" />
                        <line y1="-0.5" x2="21.2257" y2="-0.5" transform="matrix(0.753802 0.657102 0.559837 -0.828603 1 0)" stroke="white" />
                    </svg>

                </p>
            </div>
        );
    });

    return (
        <div className={classes.container}>
            <div className={classes['flex-wrapper']}>
                <div className={`${classes.item} ${classes['first-title']}`}>
                    <p>PORTFOLIO GALLERIES</p>
                </div>
                {blocks}
                <div className={`${classes.item} ${classes['last-title']}`}>
                    <p>LETS MAKE ART TOGETHER.</p>
                </div>
            </div>
            <p className={classes.remark}>
                Booking for <span>2023/2024</span> is already open!
                <br /> Get in touch with us to book your special date!
            </p>
        </div>
    );
}

export default PortfolioBlock;