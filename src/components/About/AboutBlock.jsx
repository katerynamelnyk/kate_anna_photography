import Image from 'next/image';
import localFont from 'next/font/local';

import classes from './AboutBlock.module.css';

const myFont = localFont({ src: '../../../public/SCRIPTIN.ttf' });

const AboutBlock = () => {
    const textAnna = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
    const textKate = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
    const nameClasses = myFont.className + ' ' + classes.name;

    return (
        <div className={classes.container}>
            <div className={classes.wrapper}>
                <div className={classes['item-wrapper']}>
                    <h2>ABOUT US</h2>
                    <div className={classes.text}>{textAnna}</div>
                    <div className={classes['image-block']}>
                        <Image
                            className={classes.img}
                            src='https://storage.googleapis.com/kateryn_melnyk/Anna.JPG'
                            width={400}
                            height={580}
                            alt="Anna"
                        />
                        <div className={nameClasses}>Anna</div>
                    </div>
                </div>
                <div className={classes['item-wrapper']}>
                    <div className={classes['image-block']}>
                        <Image
                            className={classes.img}
                            src='https://storage.googleapis.com/kateryn_melnyk/Kate.JPG'
                            width={400}
                            height={580}
                            alt="Kate"
                        />
                        <div className={nameClasses}>Kate</div>
                    </div>
                    <div className={classes.text}>{textKate}</div>
                    <button className={classes.button}>GET IN TOUCH</button>
                </div>
            </div>
        </div>
    )
}

export default AboutBlock;