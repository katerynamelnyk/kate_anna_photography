import PriceItem from './PriceItem';

import classes from './InformationBlock.module.css';

const weddingPrices = [
    {
        id: 'p1',
        name: 'Half-day package',
        price: '€ 1200',
        details: '- Up to 7 hours coverage- 400+ edited photographs- Engagement shoot- Online gallery- Mini-album with 30 pages- Incl VAT and travel costs in NL',
        additional: 'Additional hour: EUR 150. More photobook options are available upon request.',
    },
    {
        id: 'p2',
        name: 'Full-day package',
        price: '€ 1700',
        details: '- Up to 12 hours coverage- 800+ edited photographs- Engagement shoot- Online gallery- Fine-art wedding album with 50 pages- Incl VAT and travel costs in NL',
        additional: 'Additional hour: EUR 150. More photobook options are available upon request.',
    },
];

const lifestylePrices = [
    {
        id: 'p3',
        name: 'Lifestyle session (couples, families, maternity...) ',
        price: '€ 250',
        details: '- Up to 2 hours of photo shoot (can be divided between several locations)',
        additional: '- Styling advice- 100+ edited photographs- Online gallery- Incl VAT but excl travel costs',
    },
    {
        id: 'p4',
        name: 'Lifestyle session (couples, families, maternity...) ',
        price: '€ 180',
        details: '- Up to 1 hour of photo shoot (can be divided between several locations)',
        additional: '- Styling advice- 50+ edited photographs- Online gallery- Incl VAT but excl travel costs',
    },
];

const InformationBlock = () => {
    const weddings = weddingPrices.map(item => {
        return <PriceItem
            key={item.id}
            name={item.name}
            price={item.price}
            details={item.details}
            additional={item.additional}
        />
    });
    const lifestyles = lifestylePrices.map(item => {
        return <PriceItem
            key={item.id}
            name={item.name}
            price={item.price}
            details={item.details}
            additional={item.additional}
            isLifestyle={true}
        />
    });

    return (
        <div className={classes.container}>
            <h2>INFORMATION & PRICES</h2>
            <div className={classes['prices-wrapper']}>
                {weddings}
                <p className={classes.remark}>WEDDING PACKAGES</p>
                {lifestyles}
                <p className={classes.remark}>LIFESTYLE SHOOTS</p>
            </div>
        </div>
    )
}

export default InformationBlock;