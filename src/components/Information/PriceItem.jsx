import localFont from 'next/font/local';

import classes from './PriceItem.module.css';

const myFont = localFont({ src: '../../../public/SCRIPTIN.ttf' });

const PriceItem = (props) => {
    const headerClasses = props.isLifestyle 
        ? `${myFont.className} ${classes.lifestyle}`
        : `${myFont.className} ${classes.wedding}`;

    return (
        <div className={classes.container}>
            <h3 className={headerClasses}>
                {props.name}
            </h3>
            <div className={classes.price}>
                {props.price}
            </div>
            <div className={classes.details}>
                {props.details}
            </div>
            <div className={classes.additional}>
                {props.additional}
            </div>
        </div>
    )
}

export default PriceItem;